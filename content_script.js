/**
 * @author sunnz <gitlab.com/sunnz>
 * @license MIT
 */

(function () {
    const scriptElement = document.createElement('script')
    scriptElement.innerHTML = 'document.interestCohort = async () => ({ id: "42", version: "chrome.1.0" })'
    document.documentElement.prepend(scriptElement)
    // remove element so it is not visible to scripts on the page
    document.documentElement.removeChild(scriptElement)
})()
