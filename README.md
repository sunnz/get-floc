get-floc
========

Provides floc cohort id for Firefox so that users can opt-in take part in the floc trial on Firefox.
Currently this always sets the cohort id to "42".

What is floc? From google's own words: "Federated learning of cohorts (floc) provides a privacy-preserving mechanism
for interest-based ad selection". However only chrome will support the floc api, so this extension provides Firefox
user to opt-in to floc.

Currently the cohort id is always set to "42". In a future release I want to allow the user to select any cohort they like, and even an option to mimic chrome's cohort selection. The goal is and will always be giving the choice to the user.

After installing visit this site to check that it is getting a cohort id of "42": https://floc.glitch.me/

**Permission**

Access your data for all websites: This is required so that this extension can add the relevant
``interestCohort() `` function into the document object in order to implement the floc api.
It does this via a content script that never actually read any data out of the sites you visit.


Installation
------------

Please install via the Firefox add-ons website: https://addons.mozilla.org/en-GB/firefox/addon/get-floc/

License
-------

[MIT](./LICENSE)
